Install instructions:
======-=-=-==========

* changed to support git tag, and currently support only git tags, pull (to a branch) will be readded in future.
** backup your site and system before using this setup.
*** use at your own risk !

This module has been tested with Linux.

0. add to settings.php  $conf['gittag_git_user'] = USERNAME; 
set value with the username owning the code files.

1. create a user called gitpull.
this user must have the correct permissions to pull from the Git repository, and to update the site's code files on the web server.

2. add the webserver's user to /etc/sudoers, to allow it to git pull (www-data on Ubuntu Linux)
use same username as in settings.php:
example -
www-data ALL=(gitpull) NOPASSWD: /usr/bin/git pull, /usr/bin/git tag -l, /usr/bin/git describe --tags, /usr/bin/git fetch --tags, /usr/bin/git checkout [[\:alpha\:]]*